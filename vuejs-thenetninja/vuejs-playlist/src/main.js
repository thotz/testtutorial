import Vue from 'vue'
import App from './App.vue'
// import Ninjas from './Ninjas.vue'

// Vue.component('temp-ninjas', Ninjas);

new Vue({
  el: '#app',
  render: h => h(App)
})
