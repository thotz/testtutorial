new Vue({
  el:'#vue-app',
  data: {
    name: 'Shaun',
    job: 'Ninja',
    website: 'http://www.google.com',
    websiteTag: '<a href="http://www.google.com">The Google Website</a>',
    age: 23,
    a: 0,
    b: 0,
    available: false,
    nearby: false,
    error: false,
    success: false,
    // 12
    characters:['Mario','Luigi','Yoshi','Bowser'],
    ninjas: [
      {name:'Ryu', age: 25},
      {name:'Yoshi', age: 35},
      {name:'Ken', age: 55}
    ],
    // 13
    health: 100,
    ended: false
  },
  methods: {
    greet:function(time) {
      return 'Good '+ time + ' ' + this.name;
    },
    addAge: function(increment) {
      this.age += increment;
    },
    subAge: function(decrement) {
      this.age -= decrement;
    },
    // 13
    punch: function() {
      this.health -= 10;
      if (this.health <= 0) {
        this.ended = true;
      }
      console.log(this.health);
    },
    restart: function() {
      this.health = 100;
      this.ended = false;
    }

  },
  computed: {
    addToA: function(){
      console.log("addToA");
      return this.a + this.age;
    },
    addToB: function(){
      console.log("addToB");
      return this.b + this.age;
    },
    compClasses: function() {
      return {
        available: this.available,
        nearby: this.nearby
      }
    }
  }
});
